const express = require('express');

const app = express();
const helmet = require('helmet');
const consign = require('consign');
const bodyParser = require('body-parser');
const cors = require('../server/middlewares/cors');

app.use(helmet());
app.use(bodyParser.json());
app.use(cors);
consign({
  cwd: 'server',
})
  .include('routes')
  .into(app);

module.exports = app;
