/* eslint-disable no-console */
const config = require('config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.connect(
  config.get('mongoUrl'),
  { useNewUrlParser: true, useFindAndModify: false },
  (err) => {
    if (err) {
      return console.error('Database Connection error: ', err);
    }
    return console.log('Connected to database.');
  },
);
