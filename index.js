/* eslint-disable no-console */
const http = require('http');
const app = require('./config/express');

const port = 3000;
require('./config/database');

http.createServer(app).listen(port, () => {
  console.log(`Server started on port: ${port}`);
});
