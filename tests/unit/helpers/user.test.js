/* eslint-disable no-underscore-dangle */
const _ = require('lodash');
const userHelper = require('../../../server/helpers/user');

describe('user', () => {
  describe('clearUser', () => {
    it('should return a clean user omiting password', () => {
      const user = {
        _id: '123456',
        createdAt: new Date(),
        updatedAt: new Date(),
        password: 'super secret passsword',
        email: 'email@email.com',
      };
      const expected = {
        _id: user._id,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
        email: user.email,
      };
      const result = userHelper.cleanUser(user);
      expect(result).toEqual(expected);
    });
  });

  describe('validateEmail', () => {
    it('should true when email is valid', () => {
      const validEmails = [
        'luis@luis.com',
        'luisfer@luisfer.com',
        'cloud@email.com',
      ];
      _.forEach(validEmails, email => expect(userHelper.validateEmail(email)).toBe(true));
    });

    it('should false when email is invalid', () => {
      const validEmails = [
        'oi eu sou o goku',
        'naoSei',
        '2314.com',
      ];
      _.forEach(validEmails, email => expect(userHelper.validateEmail(email)).toBe(false));
    });
  });

  describe('validatePassword', () => {
    it('should return false for number argument', () => {
      const passsword = 123456;
      expect(userHelper.validatePassword(passsword)).toBe(false);
    });

    it('should return false too long or too short password length', () => {
      const shortPassword = '12';
      const longPasssword = 'ASDFASDFADFSDAFASDFASDFASDFASDFASDFASDFASDFADSF';

      _.forEach([shortPassword, longPasssword], (pass) => {
        expect(userHelper.validatePassword(pass)).toBe(false);
      });
    });

    it('should return true for valid password', () => {
      const passsword = '12345678';
      expect(userHelper.validatePassword(passsword)).toBe(true);
    });
  });
});
