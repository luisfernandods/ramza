# Ramza

API para registro de recordes de jogos. //WIP

## Commands
Command          | Effect
---              | ---
`npm install`    | Instala as dependencias do app.
`npm start`      | Inicia o servidor `index.js`
`npm test`       | Roda os testes
`npm eslint`     | Roda o scan do eslint


`Rotas:`

Com autenticação:

`GET /api/games` 

`GET /api/games/:id` 

`POST /api/games`
{

    "name": "",
    "type": "" //'action', 'adventure', 'puzzle', 'combat', 'race', 'other'
    
}

`PUT /api/games/:id`
{

    "name": "",
    "type": "" //'action', 'adventure', 'puzzle', 'combat', 'race', 'other'
    
}

`GET /api/users` 

`GET /api/users/:id`

Abertas:

// Retorna um token Bearer

`POST /api/login` 
{

    "email": "",
    "password": ""
    
}

`POST /api/users/`
{

    "fullName": "",
	"password": """,
	"email": ""
	
}

Ao iniciar o servidor certifique-se que o mongo esteja rodando.