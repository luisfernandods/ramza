const mongoose = require('mongoose');

const { Schema } = mongoose;
const { ObjectId } = mongoose.Types;

const schema = new Schema({
  user: {
    type: ObjectId, ref: 'User',
  },
  game: {
    type: ObjectId, ref: 'Game',
  },
  score: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

const Record = mongoose.model('Record', schema);
module.exports = Record;
