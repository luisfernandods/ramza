const mongoose = require('mongoose');
const mongooseBcrypt = require('mongoose-bcrypt');

const { Schema } = mongoose;
const {
  EMAIL_MAX_LENGTH,
  EMAIL_MIN_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  FULL_NAME_MAX_LENGTH,
  FULL_NAME_MIN_LENGTH,
  GENERATE_MIN_LENGTH_MESSAGE,
  GENERATE_MAX_LENGTH_MESSAGE,
  GENERATE_REQUIRED_MESSAGE,
} = require('../helpers/constants');

const schema = new Schema({
  fullName: {
    type: String,
    required: [true, GENERATE_REQUIRED_MESSAGE('FullName')],
    maxlength: [
      FULL_NAME_MAX_LENGTH,
      GENERATE_MAX_LENGTH_MESSAGE('FullName', FULL_NAME_MAX_LENGTH),
    ],
    minlength: [
      FULL_NAME_MIN_LENGTH,
      GENERATE_MIN_LENGTH_MESSAGE('FullName', FULL_NAME_MIN_LENGTH),
    ],
  },
  password: {
    type: String,
    required: true,
    maxlength: [
      PASSWORD_MAX_LENGTH,
      GENERATE_MAX_LENGTH_MESSAGE('Password', PASSWORD_MAX_LENGTH),
    ],
    minlength: [
      PASSWORD_MIN_LENGTH,
      GENERATE_MIN_LENGTH_MESSAGE('Password', PASSWORD_MIN_LENGTH),
    ],
  },
  email: {
    type: String,
    required: [true, GENERATE_REQUIRED_MESSAGE('Email')],
    unique: true,
    maxlength: [
      EMAIL_MAX_LENGTH,
      GENERATE_MAX_LENGTH_MESSAGE('Email', EMAIL_MAX_LENGTH),
    ],
    minlength: [
      EMAIL_MIN_LENGTH,
      GENERATE_MIN_LENGTH_MESSAGE('Email', EMAIL_MIN_LENGTH),
    ],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

schema.plugin(mongooseBcrypt, {
  rounds: 6,
});


const User = mongoose.model('User', schema);
module.exports = User;
