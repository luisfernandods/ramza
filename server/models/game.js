const mongoose = require('mongoose');

const { Schema } = mongoose;
const { GAME_TYPES, GAME_NAME_MAX_LENGTH } = require('../helpers/constants');

const schema = new Schema({
  name: {
    type: String, maxlength: GAME_NAME_MAX_LENGTH, required: true,
  },
  type: {
    type: String, enum: GAME_TYPES, default: 'other',
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  updatedAt: {
    type: Date,
    default: Date.now(),
  },
});

const Game = mongoose.model('Game', schema);
module.exports = Game;
