const mongoose = require('mongoose');

const { Schema } = mongoose;
const { ObjectId } = mongoose.Types;
const schema = new Schema({
  user: {
    type: ObjectId, ref: 'User',
  },
  action: {
    type: String,
  },
  data: {},
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

const Activity = mongoose.model('Activity', schema);
module.exports = Activity;
