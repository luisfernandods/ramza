const _ = require('lodash');
const gameDAO = require('./gameDAO');
const errors = require('../helpers/errors');
const { GAME_TYPES } = require('../helpers/constants');

const get = id => gameDAO.get(id);

const getAll = () => gameDAO.getAll();

const create = async ({ name, type }) => {
  if (!name) {
    throw new errors.BadRequest('Invalid name.');
  }

  if (!type || _.indexOf(GAME_TYPES, type) === -1) {
    throw new errors.BadRequest('Invalid type.');
  }
  return gameDAO.create(name, type);
};

const update = async (id, { name, type }) => {
  if (!name) {
    throw new errors.BadRequest('Invalid name.');
  }

  if (!type || _.indexOf(GAME_TYPES, type) === -1) {
    throw new errors.BadRequest('Invalid type.');
  }

  return gameDAO.update(id, name, type);
};

const remove = id => gameDAO.remove(id);

module.exports = {
  get,
  getAll,
  create,
  update,
  remove,
};
