const { ObjectId } = require('mongoose').Types;
const Game = require('../models/game');

const get = id => Game.findOne({ _id: new ObjectId(id) });

const getAll = () => Game.find({});

const create = (name, type) => Game.create(new Game({ name, type }));

const update = (id, name, type) => Game.findByIdAndUpdate(
  { _id: new ObjectId(id) }, { $set: { name, type } }, { new: true },
);

const remove = id => Game.remove({ _id: new ObjectId(id) });

module.exports = {
  get,
  getAll,
  create,
  update,
  remove,
};
