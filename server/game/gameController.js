/* eslint-disable no-underscore-dangle */
const Activity = require('../models/activity');
const gameService = require('./gameService');

const get = async (req, res) => {
  try {
    res.json(await gameService.get(req.params.id));
  } catch (error) {
    res.json(error);
  }
};

const getAll = async (req, res) => {
  try {
    res.json(await gameService.getAll());
  } catch (error) {
    res.json(error);
  }
};

const create = async (req, res) => {
  try {
    const newGame = await gameService.create(req.body);
    await Activity.create({ user: req.user._id, data: newGame, action: 'createdGame' });
    res.json(newGame);
  } catch (error) {
    res.json(error);
  }
};

const update = async (req, res) => {
  try {
    const updatedGame = await gameService.update(req.params.id, req.body);
    await Activity.create({ user: req.user._id, data: updatedGame, action: 'updatedGame' });
    res.json(updatedGame);
  } catch (error) {
    res.json(error);
  }
};

const remove = async (req, res) => {
  try {
    const deletedGame = await gameService.remove(req.params.id);
    await Activity.create({ user: req.user._id, data: deletedGame, action: 'deletedGame' });
    res.json(deletedGame);
  } catch (error) {
    res.json(error);
  }
};

module.exports = {
  get,
  getAll,
  create,
  update,
  remove,
};
