const jwt = require('jsonwebtoken');
const config = require('config');
const errors = require('../helpers/errors');

module.exports = (req, res, next) => {
  const bearerHeader = req.headers.authorization || '';

  if (!bearerHeader) {
    throw new errors.UnauthorizedError();
  }

  const bearer = bearerHeader.split(' ');
  const bearerToken = bearer[1];

  jwt.verify(bearerToken, config.get('SECRET'), (err, decoded) => {
    if (err) {
      throw new errors.UnauthorizedError();
    }
    req.user = decoded;
  });

  next();
};
