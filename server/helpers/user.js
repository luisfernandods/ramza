const _ = require('lodash');
const validator = require('email-validator');
const {
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
} = require('../helpers/constants');
/**
 * @param {string} email
 */
const validateEmail = email => validator.validate(email);

/**
 * @param {object} user
 */
const cleanUser = user => _.pick(user, ['_id', 'email', 'fullName', 'createdAt', 'updatedAt']);

/**
 * @param {string} password
 */
const validatePassword = (password) => {
  if (!password || !password.length) {
    return false;
  }
  return password.length > PASSWORD_MIN_LENGTH
    && password.length < PASSWORD_MAX_LENGTH;
};

module.exports = { validateEmail, cleanUser, validatePassword };
