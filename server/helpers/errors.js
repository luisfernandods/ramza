const util = require('util');

function NotFound(message, status) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message || 'Not Found';
  this.staus = status || '404';
}
util.inherits(NotFound, Error);

function BadRequest(message) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message || 'Bad Request';
  this.staus = '400';
}
util.inherits(BadRequest, Error);

function ValidationError(message, status) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message || 'Validation Error';
  this.staus = status || '422';
}
util.inherits(ValidationError, Error);

function UnauthorizedError(message, status) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message || 'Permission Denied';
  this.staus = status || '403';
}
util.inherits(UnauthorizedError, Error);

module.exports = {
  NotFound, BadRequest, ValidationError, UnauthorizedError,
};
