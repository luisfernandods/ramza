module.exports = {
  BASE_URL: '/api',
  GAME_TYPES: ['action', 'adventure', 'puzzle', 'combat', 'race', 'other'],

  PASSWORD_MAX_LENGTH: 20,
  PASSWORD_MIN_LENGTH: 3,

  EMAIL_MAX_LENGTH: 100,
  EMAIL_MIN_LENGTH: 3,

  FULL_NAME_MAX_LENGTH: 100,
  FULL_NAME_MIN_LENGTH: 3,

  GAME_NAME_MAX_LENGTH: 100,

  GENERATE_MIN_LENGTH_MESSAGE: (path, length) => `${path} must have at least ${length} characters.`,
  GENERATE_MAX_LENGTH_MESSAGE: (path, length) => `${path} need to have maximum ${length} characters maximum.`,
  GENERATE_REQUIRED_MESSAGE: path => `${path} is required.`,
};
