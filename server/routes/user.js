const {
  create,
  get,
  getAll,
  login,
} = require('../user/userController');
const { BASE_URL } = require('../helpers/constants');
const auth = require('../middlewares/auth');

const userRoute = `${BASE_URL}/users`;
const loginRoute = `${BASE_URL}/login`;

module.exports = (app) => {
  app.post(userRoute, create);
  app.get(`${userRoute}/:id`, auth, get);
  app.get(userRoute, auth, getAll);

  app.post(loginRoute, login);
};
