const {
  create,
  get,
  getAll,
  update,
  remove,
} = require('../game/gameController');
const { BASE_URL } = require('../helpers/constants');
const auth = require('../middlewares/auth');

const gameRoute = `${BASE_URL}/games`;

module.exports = (app) => {
  app.post(gameRoute, auth, create);
  app.get(gameRoute, auth, getAll);
  app.get(`${gameRoute}/:id`, auth, get);
  app.put(`${gameRoute}/:id`, auth, update);
  app.delete(`${gameRoute}/:id`, auth, remove);
};
