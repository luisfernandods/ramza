const userService = require('../user/userService');

const create = async (req, res) => {
  try {
    res.json(await userService.create(req.body));
  } catch (error) {
    res.json(error);
  }
};

const get = async (req, res) => {
  try {
    res.json(await userService.get(req.params.id));
  } catch (error) {
    res.json(error);
  }
};

const getAll = async (req, res) => {
  try {
    res.json(await userService.getAll());
  } catch (error) {
    res.json(error);
  }
};

const login = async (req, res) => {
  try {
    const auth = await userService.login(req.body);
    res.json(auth);
  } catch (error) {
    res.json(error);
  }
};

module.exports = {
  create,
  get,
  getAll,
  login,
};
