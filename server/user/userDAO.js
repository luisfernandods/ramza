const { ObjectId } = require('mongoose').Types;
const User = require('../models/user');


const create = user => User.create(new User(user));

const get = id => User.findOne({ _id: new ObjectId(id) }, { password: 0 });

const getAll = () => User.find({}, { password: 0 });

const findByEmail = email => User.findOne({ email });

module.exports = {
  create,
  get,
  getAll,
  findByEmail,
};
