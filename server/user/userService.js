/* eslint-disable no-underscore-dangle */
const jwt = require('jsonwebtoken');
const config = require('config');
const userDAO = require('./userDAO');
const errors = require('../helpers/errors');
const userHelper = require('../helpers/user');

const create = async ({ fullName, password, email }) => {
  if (!userHelper.validateEmail(email)) {
    throw new errors.ValidationError('Invalid e-mail');
  }
  const newUser = await userDAO.create({ fullName, password, email });

  return userHelper.cleanUser(newUser);
};

const get = id => userDAO.get(id);

const getAll = () => userDAO.getAll();

const login = async ({ email, password }) => {
  if (!userHelper.validatePassword(password) || !userHelper.validateEmail(email)) {
    throw new errors.BadRequest('Invalid email or password.');
  }

  const user = await userDAO.findByEmail(email);

  if (!user) {
    throw new errors.BadRequest('Invalid email or password.');
  }
  const isValidPassword = await user.verifyPassword(password);

  if (!isValidPassword) {
    throw new errors.BadRequest('Invalid email or password.');
  }

  const token = jwt.sign(
    {
      _id: user._id,
      fullName: user.fullName,
      email: user.email,
    },
    config.get('SECRET'),
    {
      expiresIn: '1d',
    },
  );

  return { auth: true, token };
};


module.exports = {
  create,
  get,
  getAll,
  login,
};
