#### 0.3.0
- Adicionada a rota `GET /api/games` 
- Adicionada a rota `GET /api/games/:id` 
- Adicionada a rota `POST /api/games` 
- Adicionada a rota `PUT /api/games/:id` 

#### 0.2.0
- Adicionada a rota `POST /api/login` 

#### 0.1.0
- Adicionada a rota `GET /api/users` 
- Adicionada a rota `GET /api/users/:id` 
- Adicionada a rota `POST /api/users/` 
